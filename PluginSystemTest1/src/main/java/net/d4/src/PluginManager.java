package net.d4.src;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by: Joshua Freedman on Jul 12, 2015 @ 9:23 PM.
 */
public class PluginManager {
    private static List<Listener> registered = new ArrayList<>();

    private static List<Class<?>> registeredBases = new ArrayList<>();

    public synchronized static void register(Listener listener) {
        System.out.println("Has been registered.");
        if (!registered.contains(listener)) {
            registered.add(listener);
        }
    }

    public synchronized static void unregister(Listener listener) {
        if (registered.contains(listener)) {
            registered.remove(listener);
        }
    }

    public synchronized static List<Listener> getRegistered() {
        return registered;
    }

    public synchronized static void registerBase(Class<?> clz) {
        System.out.println("Has been registered.");
        if (!registeredBases.contains(clz)) {
            registeredBases.add(clz);
        }
    }

    public synchronized static void unregisterBase(Class<?> clz) {
        if (registeredBases.contains(clz)) {
            registeredBases.remove(clz);
        }
    }

    public synchronized static List<Class<?>> getRegisteredBases() {
        return registeredBases;
    }

    public synchronized static void callEvent(final Event event) {
        System.out.println("Calling Event");
        new Thread() {
            @Override
            public void run() {
                call(event);
                System.out.println("Event called from thread;");
            }
        }.start();
    }

    private synchronized static void call(final Event event) {
//        for (Class<?> clz : getRegisteredBases()) {
//            for (Method method : clz.getMethods()) {
//                EventHandler eventHandler = method.getAnnotation(EventHandler.class);
//                if (eventHandler != null) {
//                    Class<?>[] methodParams = method.getParameterTypes();
//
//                    if (methodParams.length < 1) {
//                        continue;
//                    }
//                    if (!event.getClass().getSimpleName().equals(methodParams[0].getSimpleName())) {
//                        continue;
//                    }
//                    try {
//                        method.invoke(clz.newInstance(), event);
//                    } catch (Exception exception) {
//                        exception.printStackTrace();
//                    }
//                }
//            }
//        }

        getRegisteredBases().forEach(clz -> {
            Arrays.asList(clz.getMethods()).forEach(method -> {
                EventHandler eventHandler = method.getAnnotation(EventHandler.class);
                if (eventHandler != null) {
                    Class<?>[] methodParams = method.getParameterTypes();

                    if (methodParams.length < 1) {
                        return;
                    }
                    if (!event.getClass().getSimpleName().equals(methodParams[0].getSimpleName())) {
                        return;
                    }
                    try {
                        method.invoke(clz.newInstance(), event);
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                }
            });
        });

        getRegistered().forEach(listener -> {
            Arrays.asList(listener.getClass().getMethods()).forEach(method -> {
                EventHandler eventHandler = method.getAnnotation(EventHandler.class);
                if (eventHandler != null) {
                    Class<?>[] methodParams = method.getParameterTypes();

                    if (methodParams.length < 1) {
                        return;
                    }
                    if (!event.getClass().getSimpleName().equals(methodParams[0].getSimpleName())) {
                        return;
                    }
                    try {
                        method.invoke(listener.getClass().newInstance(), event);
                    } catch (Exception exception) {
                        System.err.println(exception);
                    }
                }
            });
        });

//        for (Listener listener : getRegistered()) {
//            for (Method method : listener.getClass().getMethods()) {
//                EventHandler eventHandler = method.getAnnotation(EventHandler.class);
//                if (eventHandler != null) {
//                    Class<?>[] methodParams = method.getParameterTypes();
//
//                    if (methodParams.length < 1) {
//                        continue;
//                    }
//                    if (!event.getClass().getSimpleName().equals(methodParams[0].getSimpleName())) {
//                        continue;
//                    }
//                    try {
//                        method.invoke(listener.getClass().newInstance(), event);
//                    } catch (Exception exception) {
//                        System.err.println(exception);
//                    }
//                }
//            }
//        }
    }

}
