package net.d4.src;

import net.d4.src.detector.ADetect;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by: Joshua Freedman on Jul 12, 2015 @ 7:56 PM.
 */
public class RelaunchMain {

    public static void main(String[] args) throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        ArrayList<Class<?>> classArrayList = new ArrayList<>();

        final ADetect.TypeReporter reporter = new ADetect.TypeReporter() {
            @Override
            public void reportTypeAnnotation(Class<? extends Annotation> annotation, String className) {
                try {
                    classArrayList.add(Class.forName(className));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public Class<? extends Annotation>[] annotations() {
                return new Class[]{Plugin.class};
            }
        };

        final ADetect aDetect = new ADetect(reporter);
        aDetect.detect();

        for (Class<?> clz : classArrayList) {
            PluginManager.registerBase(clz);
        }
        Event event = new AppInitEvent();
        PluginManager.callEvent(event);
        PluginManager.callEvent(new LaunchEvent("This is atest launch value"));
    }
}
