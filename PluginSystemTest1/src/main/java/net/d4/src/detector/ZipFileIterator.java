package net.d4.src.detector;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Created by: Joshua Freedman on Jul 12, 2015 @ 6:47 PM.
 */

final class ZipFileIterator {

    private final ZipFile                         zipFile;
    private final String[]                        entryNameFilter;
    private final Enumeration<? extends ZipEntry> entries;

    private ZipEntry current;

    /**
     * Create a new {@code ZipFileIterator} instance.
     *
     * @param zipFile         The ZIP file used to iterate over all entries
     * @param entryNameFilter (optional) file name filter. Only entry names starting with
     *                        one of the specified names in the filter are returned
     */
    ZipFileIterator(final ZipFile zipFile, final String[] entryNameFilter) throws IOException {
        this.zipFile = zipFile;
        this.entryNameFilter = entryNameFilter;

        this.entries = zipFile.entries();
    }

    public ZipEntry getEntry() {
        return current;
    }

    @SuppressWarnings("emptyblock")
    public InputStream next() throws IOException {
        while (entries.hasMoreElements()) {
            current = entries.nextElement();
            if (accept(current)) {
                return zipFile.getInputStream(current);
            }
        }
        // no more entries in this ZipFile, so close ZipFile
        try {
            // zipFile is never null here
            zipFile.close();
        } catch (IOException ex) {
            // suppress IOException, otherwise close() is called twice
        }
        return null;
    }

    private boolean accept(final ZipEntry entry) {
        if (entry.isDirectory()) {
            return false;
        }
        if (entryNameFilter == null) {
            return true;
        }
        for (final String filter : entryNameFilter) {
            if (entry.getName().startsWith(filter)) {
                return true;
            }
        }
        return false;
    }

}