package net.d4.src;

import java.io.File;

/**
 * Created by: Joshua Freedman on Jul 12, 2015 @ 2:09 PM.
 */
public class MainClass {

    public static void main(String[] args) throws Exception {

        File rootFolder = new File("mods");
        if (!rootFolder.exists()) rootFolder.mkdirs();

        String classpath = "";

        for (File file : rootFolder.listFiles()) {
            System.err.println(file.getAbsolutePath());
            classpath += file.getPath() + ";";
        }

        System.out.println("PRE: " + classpath);
        classpath = classpath.substring(0, classpath.length() - 1);
        System.out.println("POST: " + classpath);

        ProcessBuilder processBuilder = new ProcessBuilder("java", "-cp", "\"mods/*;TestSNAPSHOT.jar\"", "net.d4.src.RelaunchMain");
        processBuilder.directory(rootFolder.getParentFile());
        File log = new File("log");
        processBuilder.redirectErrorStream(true);
        processBuilder.redirectOutput(ProcessBuilder.Redirect.appendTo(log));

        Process process = processBuilder.start();

        while (process.isAlive()) {

        }

        System.out.println("FINALLY COMPLETE...");

    }
}
