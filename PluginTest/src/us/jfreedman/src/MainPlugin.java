package us.jfreedman.src;

import net.d4.src.*;

/**
 * Created by: Joshua Freedman on Jul 12, 2015 @ 9:10 PM.
 */
@Plugin(name = "MyPlugin", uniqueID = "uuidindeed2")
public class MainPlugin implements Listener {

    @EventHandler
    public void launch(AppInitEvent event) {
        System.out.println("TEST: " + event.getName());
//        PluginManager.register(this);
    }

    @EventHandler
    public void callEvent(LaunchEvent event) {
        System.out.println(event.getName());
    }

}
